#!/usr/bin/env python

# README: you need python 2.7 and PILLOW python library to run the script

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

# stackoverflowed from
# https://haptik.ai/tech/putting-text-on-image-using-python/
# https://code-maven.com/create-images-with-python-pil-pillow
# https://pillow.readthedocs.io/en/3.1.x/handbook/concepts.html#concept-modes

image = Image.new('L', (550, 206), color='white')
draw = ImageDraw.Draw(image)
jifont = ImageFont.truetype('jileaf.ttf', size=165)
draw.text((5, 25), "jileaf", fill="black", font=jifont)
image.save('jileaf.png')
