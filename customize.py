#!/usr/bin/env python

from __future__ import print_function
from __future__ import with_statement

import sys
import os

try:
    import fontforge
except:
    print("please install fontforge bindings for python", file=sys.stderr)
    exit(1)


def recast_digits(basefont, digit_variant):
    print("recast digits to " + digit_variant + " type")

    if digit_variant == "normal":
        overwritefont = fontforge.open("jileaf_normal_digits.sfd")
    elif digit_variant == "digital":
        overwritefont = fontforge.open("jileaf_digital_digits.sfd")

    overwritefont.selection.all()
    for glyph in list(overwritefont.selection.byGlyphs):
        overwritefont.selection.select(glyph.encoding)
        overwritefont.copy()
        basefont.selection.select(glyph.encoding)
        basefont.paste()

    overwritefont.close()


def get_local_version():
    try:
        with open(".local_version", "r") as lversion:
            ver_str = lversion.read()
            if len(ver_str) == 0:
                ver = 0
            else:
                ver = int(ver_str)
    except:
        print("local version file not found or invalid, creating a new one",
              file=sys.stderr)
        ver = 0

    with open(".local_version", "w") as lversion:
        print(ver + 1, file=lversion)

    return ver


def main():
    basefont = fontforge.open("jileaf.sfd")

    for arg in sys.argv[1:]:
        if arg.startswith("digits="):
            argvalue = arg[7:]
            recast_digits(basefont, argvalue)

    ver = get_local_version()
    font_file_name = "jileaf-%03d" % ver
    basefont.fontname = font_file_name
    basefont.fullname = "jileaf"
    font_file_name += ".ttf"
    basefont.generate(font_file_name)
    print("%s has been written" % font_file_name)


if __name__ == "__main__":
    main()
