![](jileaf.png)

The monospaced font is developed with the purpose of greater text density.
It is not really perfect or a pretty looking font, it still has flaws.
It has less line spacing than a usual font and has some peculiar features like
shapes for i and j, all cap letters are shifted below the baseline.
Currently there are only latin letters in the font.
I will add other alphabets in foreseeable future.

I hope the font will be useful for you.
Also I hope this work will encourage other font designers to create fonts
with similar properties.
